#!/usr/bin/python

import sys

## common braille dictionary, with 2x6 column laid out in one row
braille = {
"O.....": "a",
"O.O...": "b",
"OO....": "c",
"OO.O..": "d",
"O..O..": "e",
"OOO...": "f",
"OOOO..": "g",
"O.OO..": "h",
".OO...": "i",
".OOO..": "j",
"O...O.": "k",
"O.O.O.": "l",
"OO..O.": "m",
"OO.OO.": "n",
"O..OO.": "o",
"OOO.O.": "p",
"OOOOO.": "q",
"O.OOO.": "r",
".OO.O.": "s",
".OOOO.": "t",
"O...OO": "u",
"O.O.OO": "v",
".OOO.O": "w",
"OO..OO": "x",
"OO.OOO": "y",
"O..OOO": "z"
}

# read input from stdin
data = sys.stdin.readlines()

word = ""

# translate the input into characters that match the braille dictionary above
for i in range(0, len(data[0]), 3):

    # walk through 3 columns at a time for each new character
    character = ""

    for line in data:
        # for each column set, parse through each line,
        # adding that group of 2 symbols O|. to the overall
        # character to match the format of the braille dictionary
        character += line[i:i+2]

    # add current a-z character to word
    word += braille[character]

print word
